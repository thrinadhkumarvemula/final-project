import React, { useState } from "react";
import { Query } from "react-apollo";
import { gql } from "apollo-boost";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShoppingCart, faReceipt } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

import Shopping from "../components/shopping";

const Main = ({ cartState, dispatchCart }) => {
  const [search, setSearch] = useState("");
  const { total } = cartState;


  return (
    <div>
      <div className="w-full bg-indigo-900 py-8 pl-10 pr-4 sm:pr-6 flex content-center">
        <input
          className="
            focus:outline-0
            focus:shadow-outline
            border
            border-gray-300
            rounded-lg
            py-2
            px-4
            block
            w-full
            appearance-none
            leading-normal
            mr-8
           "
          onChange={e => setSearch(e.target.value)}
          value={search}
          placeholder="Search title or description"
        />
        <Link
          className="flex whitespace-no-wrap mt-2 mr-4 text-white font-bold text-xl"
          to="/order"
        >
          <FontAwesomeIcon
            className="mt-1 mr-2"
            icon={faReceipt}
            color="white"
            size="lg"
          />
          <span className="hidden sm:inline">My Order</span>
        </Link>
        <Link
          className="relative flex whitespace-no-wrap mt-2 text-white font-bold text-xl pr-8"
          to="/cart"
        >
          <FontAwesomeIcon
            className="mt-1 mr-2"
            icon={faShoppingCart}
            color="white"
            size="lg"
          />
          <span className="hidden sm:inline">Cart</span>
          {total > 0 && (
            <div
              style={{ paddingTop: "2px" }}
              className="absolute right-0 mt-1 mr-3 sm:mr-1 bg-red-700 rounded-full border-white border-2 w-6 h-6 font-mono flex justify-center content-center text-xs"
            >
              {total}
            </div>
          )}
        </Link>
      </div>
      <Query
        query={gql`
          {
            products {
              id
              title
              description
              picture
              price
            }
          }
        `}
      >
        {({ loading, error, data }) => {
          if (loading) return <p>Loading...</p>;
          if (error) return <p>Error :(</p>;

          return (
            <Shopping
              data={data}
              search={search.toLowerCase()}
              handleAddToCart={dispatchCart}
            />
          );
        }}
      </Query>
    </div>
  );
};

export default Main;
