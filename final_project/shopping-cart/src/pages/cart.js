import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faMinusCircle } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

import CheckoutModal from "../components/checkout-modal";
import { REMOVE_FROM_CART } from "../constants/cart";

const Cart = ({ cartState: { cart }, dispatchCart, dispatchOrder }) => {
  const [openCheckoutModal, setCheckoutModal] = useState(false);
  const products = cart ? Object.values(cart) : [];
  // const products = [
  //   {
  //     id: "432",
  //     title: "PG 3",
  //     picture:
  //       "https://c.static-nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/gz1xspyyaowwcske19fx/pg-3-basketball-shoe-dxhmjL.jpg",
  //     price: 145.99,
  //     qty: 1
  //   }
  // ];

  const total = products.reduce((sum, item) => {
    sum += item.price * item.qty;
    return sum;
  }, 0);
  if (openCheckoutModal) {
    window.scrollTo(0, 0);
  }

  return (
    <div
      className={
        openCheckoutModal
          ? "bg-white p-4 sm:p-10 w-full fixed overflow-hidden"
          : "bg-white p-4 sm:p-10"
      }
      style={{ minHeight: "150px" }}
    >
      {openCheckoutModal && (
        <CheckoutModal
          products={products}
          total={total}
          handleModalClose={setCheckoutModal}
          handleCheckout={dispatchOrder}
          handleCartReset={dispatchCart}
        />
      )}
      <Link to="/" className="flex content-center mb-8 text-indigo-800">
        <FontAwesomeIcon className="mt-2 mr-2" icon={faArrowLeft} size="lg" />
        <span className="font-bold text-2xl">Back</span>
      </Link>
      <h1 className="text-indigo-800 text-5xl font-bold mb-8">Shopping Cart</h1>

      {products.length === 0 ? (
        <h3 className="text-gray-500 text-lg font-bold">Cart is empty</h3>
      ) : (
        <div>
          <h2 className="mb-16 text-4xl font-bold w-full text-right">
            Total: $ {total}
          </h2>
          {products.map(({ id, title, price, qty, picture }) => (
            <div
              key={id}
              className="relative w-full flex mb-4 rounded shadow-lg"
            >
              <FontAwesomeIcon
                onClick={() => dispatchCart({ type: REMOVE_FROM_CART, id })}
                className="absolute top-0 right-0 mr-16 mt-16"
                icon={faMinusCircle}
                color="#c53030"
                size="2x"
              />
              <img src={picture} alt={title} className="w-32" />
              <div className="p-8 w-full bg-white flex flex-col justify-between leading-normal">
                <h1 className="text-5xl font-bold text-indigo-800">{title}</h1>
                <div className="text-lg font-bold">
                  <span className="text-2xl">$ {price}</span> x{" "}
                  <span className="text-2xl">{qty}</span>
                </div>
              </div>
            </div>
          ))}
          <button
            className="mt-8 w-full font-bold bg-green-600 hover:bg-green-700 rounded-lg p-4 text-white text-lg"
            onClick={() => setCheckoutModal(true)}
          >
            CHECKOUT
          </button>
        </div>
      )}
    </div>
  );
};

export default Cart;
