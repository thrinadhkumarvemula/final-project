import React, { useReducer } from "react";
import ReactDOM from "react-dom";
import { ApolloProvider } from "react-apollo";
import { BrowserRouter, Route } from "react-router-dom";
import "./styles/tailwind.css";

import client from "./apollo-client";
import Main from "./pages/main";
import Cart from "./pages/cart";
import Order from "./pages/order";
import { ADD_TO_CART, RESET_CART, REMOVE_FROM_CART } from "./constants/cart";
import { ADD_TO_ORDER } from "./constants/order";

const cartInitialState = { cart: {}, total: 0 };

function cartReducer(state, action) {
  switch (action.type) {
    case ADD_TO_CART: {
      const { id } = action.payload;
      if (state.cart[id]) {
        return {
          cart: {
            ...state.cart,
            [id]: { ...action.payload, qty: state.cart[id].qty + 1 }
          },
          total: state.total + 1
        };
      }
      return {
        cart: {
          ...state.cart,
          [id]: { ...action.payload, qty: 1 }
        },
        total: state.total + 1
      };
    }
    case REMOVE_FROM_CART: {
      const { id } = action;
      if (state.cart[id]) {
        delete state.cart[id];
        return { ...state, cart: { ...state.cart }, total: state.total - 1 };
      }
      return state;
    }
    case RESET_CART:
      return cartInitialState;
    default:
      throw new Error();
  }
}

const orderInitialState = [];

function orderReducer(state, action) {
  switch (action.type) {
    case ADD_TO_ORDER: {
      state.push(action.payload);
      console.log(state);
      return [...state];
    }
    default:
      throw new Error();
  }
}

function App() {
  const [cartState, dispatchCart] = useReducer(cartReducer, cartInitialState);
  const [orderState, dispatchOrder] = useReducer(
    orderReducer,
    orderInitialState
  );

  return (
    <ApolloProvider client={client}>
      <BrowserRouter>
        <Route
          path="/"
          exact
          render={props => (
            <Main
              {...props}
              cartState={cartState}
              dispatchCart={dispatchCart}
            />
          )}
        />
        <Route
          path="/order"
          render={props => <Order {...props} orderState={orderState} />}
        />
        <Route
          path="/cart"
          render={props => (
            <Cart
              {...props}
              cartState={cartState}
              dispatchCart={dispatchCart}
              dispatchOrder={dispatchOrder}
            />
          )}
        />
      </BrowserRouter>
    </ApolloProvider>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
