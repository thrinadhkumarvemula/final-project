import React from "react";

import Product from "./product";

const ProductList = ({ products, handleAddToCart }) => (
  <div className="flex flex-col sm:flex-row flex-wrap -mb-4">
    {products.map(({ id, title, description, picture, price }) => (
      <Product
        key={id}
        id={id}
        title={title}
        description={description}
        picture={picture}
        price={price}
        handleAddToCart={handleAddToCart}
      />
    ))}
  </div>
);

export default ProductList;
