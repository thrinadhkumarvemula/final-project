import React, { useState } from "react";
import { withRouter } from "react-router";

import { ADD_TO_ORDER } from "../constants/order";
import { RESET_CART } from "../constants/cart";

const CheckoutModal = ({
  handleModalClose,
  products,
  total,
  handleCheckout,
  handleCartReset,
  history
}) => {
  const [email, setEmail] = useState("");

  return (
    <div className="w-full h-full flex justify-center items-center overflow-hidden">
      <div className="absolute left-0 top-0 bg-gray-700 opacity-50 w-full h-full z-10" />
      <div className="absolute mt-64 w-3/4 flex flex-col justify-center bg-white opacity-100 z-20 p-16 rounded-lg">
        <div className="md:flex md:items-center mb-6">
          <div className="md:w-1/3">
            <label
              className="relative block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4"
              for="email"
            >
              Email
              <span className="ml-1 absolute text-red-600 text-xs">*</span>
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              id="email"
              type="email"
              placeholder="johndoe@gmail.com"
              value={email}
              onChange={e => setEmail(e.target.value)}
            />
          </div>
        </div>
        <div className="mt-4 w-full flex justify-center content-center">
          <div className="flex items-center">
            <button
              className="w-full px-16 py-4 rounded-lg bg-gray-300 hover:bg-gray-400 text-black font-bold mr-8"
              onClick={() => {
                setEmail("");
                handleModalClose(false);
              }}
            >
              CANCEL
            </button>
            <button
              className={
                !!email
                  ? "w-full px-16 py-4 rounded-lg bg-indigo-800 hover:bg-indigo-900 text-white font-bold whitespace-no-wrap"
                  : "w-full px-16 py-4 rounded-lg bg-indigo-800 hover:bg-indigo-900 text-white font-bold whitespace-no-wrap opacity-50 cursor-not-allowed"
              }
              disabled={!email}
              onClick={() => {
                handleCheckout({
                  type: ADD_TO_ORDER,
                  payload: {
                    items: products,
                    total,
                    email,
                    createAt: new Date()
                  }
                });
                setEmail("");
                handleModalClose(false);
                handleCartReset({
                  type: RESET_CART
                });
                history.push("/order");
              }}
            >
              COMPLETE ORDER
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(CheckoutModal);
