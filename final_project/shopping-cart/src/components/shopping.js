import React from "react";
import ProductList from "./product-list";

const Shopping = ({ data: { products }, search, handleAddToCart }) => {
  return (
    <div>
      <ProductList
        products={products.filter(
          ({ title, description }) =>
            title.toLowerCase().includes(search) ||
            description.toLowerCase().includes(search)
        )}
        handleAddToCart={handleAddToCart}
      />
    </div>
  );
};

export default Shopping;
