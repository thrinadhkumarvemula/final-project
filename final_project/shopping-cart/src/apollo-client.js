import ApolloClient from "apollo-boost";

const client = new ApolloClient({
  uri: "https://yhmsj.sse.codesandbox.io"
});

export default client;
