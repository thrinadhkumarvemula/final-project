import React from 'react';
import axios from 'axios';
class RestApi extends React.Component {
    constructor() {
        super();
        this.state = {
            rest: []
        }
    }
    componentDidMount() {
        console.log("componentDidMount");

        axios.get('http://127.0.0.1:8000/products/').then(res => {
            const rest = res.data;
            this.setState({ rest });
            console.log(res.data)
        });


    }
    render() {
        return (
            <div>

                <h3>hello world</h3>
                <h5>RestApi Data</h5>
                <div className="bg-success text-white">
                    <ul> {this.state.rest.map((p, index) => (

                        <li key={index}>
                            {p.id}<br/>

                            {p.title}<br/>

                            {p.price}<br/>
                            {p.description}<br/>

                        </li>
                    ))}
                    </ul>

                    <p>Data from RestApi</p>
                </div>
                </div>
        );
    }
}
export default RestApi;