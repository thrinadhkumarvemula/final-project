"""shoppingonline URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
"""shopping URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from knox import views as knox_views
from ecommerce import serializersviews
from ecommerce.serializersviews import userviews, RegisterView,ChangePasswordView
from rest_framework import routers
router=routers.DefaultRouter()
router.register('register',userviews)

urlpatterns = [
    path('admin/', admin.site.urls),
    # product Rest-framework
    path("products/", serializersviews.products.as_view()),
    # path("products/<username>", serializersviews.TodoList.as_view()),
    path("products/<int:pk>", serializersviews.productsDetails.as_view()),

    # ORDERS Rest-framework

    path("orders/", serializersviews.Order.as_view()),
    # path("orders/<username>", serializersviews.Order.as_view()),
    path("orders/<int:pk>", serializersviews.OrderDetails.as_view()),

    # ORDERSITEMS Rest-framework

    path("ordersitems/", serializersviews.OrderitemsList.as_view()),
    # path("ordersitems/<username>", serializersviews.OrderitemsList.as_view()),
    path("ordersitems/<int:pk>", serializersviews.OrderitemsDetails.as_view()),


    path('user',include(router.urls)),
    path('account/register/', RegisterView.as_view(), name='auth_register'),
    path('account/change_password/<int:pk>/', ChangePasswordView.as_view(), name='auth_change_password'),
#     path('api/register/', RegisterAPI.as_view(), name='register'),
#     path('api/login/', LoginAPI.as_view(), name='login'),
#     path('api/logout/', knox_views.LogoutView.as_view(), name='logout'),
#     path('api/logoutall/', knox_views.LogoutAllView.as_view(), name='logoutall')

]