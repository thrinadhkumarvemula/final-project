from rest_framework import serializers

from .models import Products
from .models import Orders
from .models import OrdersItems
from rest_framework.authtoken.models import Token




class Myserializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = ('Title', 'Description', 'image','Added_Date', 'Updated_Date','Price')



class MyserializerOrders(serializers.ModelSerializer):
    class Meta:
        model = Orders
        fields = ('Total', 'user_id', 'Added_Date', 'Updated_Date','Mode_of_payments','status')




class MyserializerOrdersItems(serializers.ModelSerializer):
    class Meta:
        model = OrdersItems
        fields = ('quantity', 'order_id', 'product_id', 'price')



from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password

# User Serializer
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')

# Register Serializer
# class RegisterSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = User
#         fields = ('id', 'username', 'email', 'password')
#         extra_kwargs = {'password': {'write_only': True}}
#
#     def create(self, validated_data):
#         user = User.objects.create_user(validated_data['username'], validated_data['email'], validated_data['password'])
#         Token.objects.create(user=user)
#         return user

class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )

    Password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    Confirm_Password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('username', 'Password', 'Confirm_Password', 'email')


    def validate(self, attrs):
        if attrs['Password'] != attrs['Confirm_Password']:
            raise serializers.ValidationError({"Password": "Password fields didn't match."})

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],

        )

        user.set_password(validated_data['Password'])
        user.save()

        return user



class ChangePasswordSerializer(serializers.ModelSerializer):
    Password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    Confirm_Password = serializers.CharField(write_only=True, required=True)
    old_password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('old_password', 'Password', 'Confirm_Password')

    def validate(self, attrs):
        if attrs['Password'] != attrs['Confirm_Password']:
            raise serializers.ValidationError({"Password": "Password fields didn't match."})

        return attrs

    def validate_old_password(self, value):
        user = self.context['request'].user
        if not user.check_password(value):
            raise serializers.ValidationError({"Previous_password": "Old password is not correct"})
        return value

    def update(self, instance, validated_data):
        user = self.context['request'].user

        if user.pk != instance.pk:
            raise serializers.ValidationError({"authorize": "You dont have permission for this user."})

        instance.set_password(validated_data['Password'])
        instance.save()

        return instance