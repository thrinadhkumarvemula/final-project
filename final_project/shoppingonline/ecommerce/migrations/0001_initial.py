# Generated by Django 2.2 on 2021-01-07 04:22

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Orders',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Total', models.IntegerField()),
                ('Added_Date', models.DateTimeField(auto_now_add=True)),
                ('Updated_Date', models.DateTimeField(auto_now=True)),
                ('status', models.CharField(choices=[('new', 'New'), ('paid', 'Paid')], max_length=20)),
                ('Mode_of_payments', models.CharField(choices=[('cash', 'Cash'), ('paytm', 'Paytm'), ('card', 'Card')], max_length=30)),
                ('user_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Products',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Title', models.CharField(max_length=200, null=True)),
                ('Description', models.CharField(max_length=300, null=True)),
                ('image', models.CharField(max_length=1000)),
                ('Added_Date', models.DateField(auto_now_add=True)),
                ('Updated_Date', models.DateField(auto_now=True)),
                ('Price', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='OrdersItems',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField()),
                ('price', models.FloatField()),
                ('order_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ecommerce.Orders')),
                ('product_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ecommerce.Products')),
            ],
        ),
    ]
