from django.contrib.auth.models import User
from rest_framework.authentication import BasicAuthentication, TokenAuthentication

from .models import Products
from .models import Orders
from .models import OrdersItems
from .serializers import Myserializer,RegisterSerializer
from .serializers import MyserializerOrders
from .serializers import MyserializerOrdersItems
from rest_framework import generics
from rest_framework.permissions import IsAdminUser, IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework import pagination,viewsets
from django.contrib.auth.models import User
from .serializers import RegisterSerializer, ChangePasswordSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny



class RegisterView(generics.CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer

class ChangePasswordView(generics.UpdateAPIView):
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = ChangePasswordSerializer
class products(generics.ListCreateAPIView):
    queryset = Products.objects.all()
    serializer_class = Myserializer
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]

class productsDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Products.objects.all()
    serializer_class = Myserializer
    # # authentication_classes = [TokenAuthentication]
    # # permission_classes = [IsAuthenticated]



class Order(generics.ListCreateAPIView):
    queryset = Orders.objects.all()
    serializer_class = MyserializerOrders
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

class OrderDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Orders.objects.all()
    serializer_class = MyserializerOrders
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]


class OrderitemsList(generics.ListCreateAPIView):
    queryset = OrdersItems.objects.all()
    serializer_class = MyserializerOrdersItems
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]

class OrderitemsDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Orders.objects.all()
    serializer_class = MyserializerOrdersItems
    authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]
class userviews(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class=RegisterSerializer
